# hayai
alexis summer hale

hayai is an algorithm for mapping arbitrary integers to a series of pronounceable consonants and vice-versa. the current implementation is pretty rough and needs to be librarified.

developed as a potential way to speed up unicode input, especially for emojis.

## dependencies
hayai has no dependencies outside the C standard library.

## use
to compile, run "make". type an integer and press enter to convert to a pronounceable string. type a valid string to convert it back to an integer.
