#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>
#include <string.h>

typedef unsigned char byte;
struct consonant {
	char ch;
	byte val;
};
typedef char vowel [2];

const vowel vowels[] = {
	{'a',0},
	{'i',0},
	{'u',0},
	{'a','i'},
	{'a','u'},
	{'i','u'},
	{'i','a'},
};

#if defined __clang__ || defined __llvm__
	const size_t vowelc = sizeof(vowels)/sizeof(vowel);
#else
	#define vowelc (sizeof(vowels)/sizeof(vowel))
#endif

const struct consonant consonants[] = {
	{'m', vowelc*0},
	{'n', vowelc*1},
	{'p', vowelc*2},
	{'t', vowelc*3},
	{'k', vowelc*4},
	{'b', vowelc*5},
	{'d', vowelc*6},
	{'g', vowelc*7},
};


const struct consonant* get_consonant(byte seg) {
	const size_t cc = sizeof(consonants)/sizeof(struct consonant);
	for(size_t i = 0;i<cc;++i) {
		if (seg>=consonants[i].val && (i==cc-1 || seg<consonants[i+1].val)) {
			return &consonants[i];
		}
	}
	return 0;
}
bool tr_consonant(char c, int* dst) {
	for(size_t i = 0;i<sizeof(consonants)/sizeof(struct consonant);++i) {
		if (c==consonants[i].ch) {
			*dst = consonants[i].val;
			return true;
		}
	}
	return false;
}
char* tr_vowel(char* c, int* dst) {
	for(size_t i = sizeof(vowels)/sizeof(vowel);; --i) {
		if (i > 0 && vowels[i][1] != 0) {
			if (c[0] == vowels[i][0] && c[1] == vowels[i][1]) {
				*dst = i;
				return c + 2;
			}
		} else {
		  	if (*c == vowels[i][0]) {
		  		*dst = i;
				return c + 1;
		  	}
		}
		if (i==0) break;
	}
	return NULL;
}
char* buf_syl(const struct consonant* c, const vowel* v, char* out) {
	*(out++) = c->ch;
	*(out++) = (*v)[0];
	if ((*v)[1]!=0) *(out++) = (*v)[1];
	return out;
}

char* encode(unsigned int cp) {
	char buf[13], *curs=buf; // max size is 12 letters
	start:;
		byte seg = cp % 56;
		const struct consonant* c = get_consonant(seg);
		if (!c) printf("something went wrong! D:\n"), exit(1);
		seg -= c->val; // seg now codes only the vowel
		const vowel* v = vowels + seg;
		curs=buf_syl(c,v,curs);
	if(cp>56) {cp/=56; goto start;}
	*curs=0;
	char* str = malloc(sizeof(char)*strlen(buf));
	strcpy(str,buf);
	return str;
}
unsigned int decode(char* code) {
	unsigned int r = 0, mult = 1;
	for (size_t i = 0;;++i) {
		unsigned int place = 0;
		/* decode consonant */ {
			char consonant = *code;
			if (consonant == 0) return r;
			int base;
			if (tr_consonant(consonant, &base)) {
				place = base;
				++code;
			} else {
				printf("%c is not a consonant used in this encoding!\n", consonant);
				exit(1);
			}
		}
		if (*code == 0) {printf("unexpected end of string!\n"); exit(1);}
		/* decode vowel */ {
			char* vowel = code;
			int add;
			if ((code = tr_vowel(vowel, &add))) {
				place += add;
			} else {
				printf("%c is not a vowel used in this encoding!\n", *vowel);
				exit(1);
			}
		}
		r += place*mult;
		mult = mult * 56;
		if (*code == 0 || *code == '\n' || *code == '\t' || *code == ' ') return r;
	}
}
int main() {
	for (;;) {
		char buf[50];
		buf[0]=0;
		unsigned int cp;
		printf("💬  ");
		fgets(buf,50,stdin);
		if (buf[0] == '\n') exit(1);
		if (sscanf(buf,"%d", &cp)) {
			char* c = encode(cp);
			printf("🔠  \e[1m%s\e[0m\n\n", c);
			free(c);
		} else {
			cp = decode(buf);
			printf("🔢  \e[1m%d\e[0m\n\n",cp);
		}
	}
}
